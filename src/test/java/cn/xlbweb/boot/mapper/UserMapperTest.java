//package cn.xlbweb.boot.mapper;
//
//import cn.xlbweb.boot.AppTests;
//import cn.xlbweb.boot.model.User;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.UUID;
//
//public class UserMapperTest extends AppTests {
//
//    @Autowired
//    private UserMapper userMapper;
//
//    @Test
//    public void countByExample() {
//    }
//
//    @Test
//    public void deleteByExample() {
//    }
//
//    @Test
//    public void deleteByPrimaryKey() {
//    }
//
//    @Test
//    public void insert() {
//    }
//
//    @Test
//    public void insertSelective() {
//        User user = new User();
//        user.setId(UUID.randomUUID().toString());
//        user.setUsername("zhangsan");
//        user.setPassword("111111");
//        int count = userMapper.insertSelective(user);
//        if (count > 0) {
//            System.out.println("插入成功");
//        } else {
//            System.out.println("插入失败");
//        }
//    }
//
//    @Test
//    public void selectByExample() {
//    }
//
//    @Test
//    public void selectByPrimaryKey() {
//        User user = userMapper.selectByPrimaryKey("abcdefg");
//        System.out.println(user);
//    }
//
//    @Test
//    public void updateByExampleSelective() {
//    }
//
//    @Test
//    public void updateByExample() {
//    }
//
//    @Test
//    public void updateByPrimaryKeySelective() {
//        User user = new User();
//        user.setId("577f449c-7342-4c82-b7d1-0c20bb354e2d");
//        user.setUsername("zhangsanxx");
//        user.setPassword("111111");
//        int count = userMapper.updateByPrimaryKeySelective(user);
//        if (count > 0) {
//            System.out.println("更新成功");
//        } else {
//            System.out.println("更新失败");
//        }
//    }
//
//    @Test
//    public void updateByPrimaryKey() {
//    }
//}