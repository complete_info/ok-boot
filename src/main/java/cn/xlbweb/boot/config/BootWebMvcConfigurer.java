package cn.xlbweb.boot.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: bobi
 * @date: 2019-08-19 22:06
 * @description:
 */
@Configuration
public class BootWebMvcConfigurer implements WebMvcConfigurer {

}
